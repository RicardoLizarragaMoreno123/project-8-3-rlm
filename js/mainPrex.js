function buscarUsuario() {
    const userId = document.getElementById('userId').value;
  
    // Verificar si el ID ingresado es válido
    if (userId && userId > 0) {
      // Verificar si el ID es mayor que 10
      if (userId > 10) {
        alert('Solo se permiten IDs hasta 10. Por favor, ingrese un ID válido.');
        return;
      }
  
      // Realizar la petición a la API
      fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(response => {
          // Verificar si la respuesta es exitosa (código 200)
          if (!response.ok) {
            throw new Error('Error en la respuesta de la API');
          }
          return response.json();
        })
        .then(data => mostrarInformacionUsuario(data))
        .catch(error => console.error('Error al obtener la información del usuario', error));
    } else {
      alert('Ingrese un ID de usuario válido.');
    }
  }
  
  function mostrarInformacionUsuario(usuario) {
    const userInfoContainer = document.getElementById('userInfo');
    userInfoContainer.innerHTML = `
      <h2>Información del Usuario</h2>
      <p><strong>Nombre:</strong> ${usuario.name}</p>
      <p><strong>Nombre de Usuario:</strong> ${usuario.username}</p>
      <p><strong>Email:</strong> ${usuario.email}</p>
      <p><strong>Dirección:</strong> ${usuario.address.street}, ${usuario.address.suite}, ${usuario.address.city}</p>
    `;
  }
  
  function limpiarInfo() {
    const userInfoContainer = document.getElementById('userInfo');
    userInfoContainer.innerHTML = '';
    document.getElementById('userId').value = '';
  }
