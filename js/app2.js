function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";

    http.onreadystatechange = function() {
        if (this.readyState === 4) {
            if (this.status === 200) {
                let res = document.getElementById('lista');
                res.innerHTML = ""; // Clear existing data

                const json = JSON.parse(this.responseText);

                // Create a DocumentFragment to improve rendering
                let fragment = document.createDocumentFragment();

                // Construct all rows
                for (const datos of json) {
                    let row = document.createElement('tr');
                    row.innerHTML = '<td class="columna1">' + datos.userId + '</td>' +
                        '<td class="columna2">' + datos.id + '</td>' +
                        '<td class="columna3">' + datos.title + '</td>';

                    fragment.appendChild(row);
                }

                // Append all rows to the table at once
                res.appendChild(fragment);
            } else {
                console.error("Error making the request");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

function buscarPorId() {
    const id = document.getElementById('inputId').value;
    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/albums/${id}`;

    http.onreadystatechange = function() {
        if (this.readyState === 4) {
            if (this.status === 200) {
                let res = document.getElementById('lista');
                res.innerHTML = ""; // Clear existing data

                const json = JSON.parse(this.responseText);

                let row = document.createElement('tr');
                row.innerHTML = '<td class="columna1">' + json.userId + '</td>' +
                    '<td class="columna2">' + json.id + '</td>' +
                    '<td class="columna3">' + json.title + '</td>';

                // Append the row to the table
                res.appendChild(row);
            } else {
                console.error("Error making the request");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnBuscar").addEventListener('click', buscarPorId);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});