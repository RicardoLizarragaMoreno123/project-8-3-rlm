document.addEventListener('DOMContentLoaded', function() {
   const breedSelect = document.getElementById('breedSelect');
   const dogImageContainer = document.getElementById('dogImageContainer');
   const dogImage = document.getElementById('dogImage');
   const loadButton = document.getElementById('loadButton');
   const showButton = document.getElementById('showButton');

   // Obtener lista de razas de perros y llenar el select
   fetch('https://dog.ceo/api/breeds/list')
      .then(response => response.json())
      .then(data => {
         data.message.forEach(breed => {
            const option = document.createElement('option');
            option.value = breed;
            option.textContent = breed;
            breedSelect.appendChild(option);
         });
      })
      .catch(error => console.error('Error al obtener la lista de razas', error));

   // Función para cargar la imagen al hacer clic en "Cargar Raza"
   loadButton.addEventListener('click', () => {
      const selectedBreed = breedSelect.value;
      if (selectedBreed) {
         alert(`Raza seleccionada: ${selectedBreed}`);
      } else {
         alert('Por favor, selecciona una raza antes de cargar.');
      }
   });

   // Función para mostrar la imagen al hacer clic en "Mostrar Imagen"
   showButton.addEventListener('click', () => {
      const selectedBreed = breedSelect.value;
      if (selectedBreed) {
         // Obtener imagen aleatoria de la raza seleccionada
         fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
            .then(response => response.json())
            .then(data => {
               dogImage.src = data.message;
               dogImage.alt = `Imagen de un perro de raza ${selectedBreed}`;
               dogImageContainer.style.display = 'block'; // Mostrar la imagen
            })
            .catch(error => console.error('Error al cargar la imagen del perro', error));
      } else {
         alert('Por favor, selecciona una raza antes de mostrar la imagen.');
      }
   });
});