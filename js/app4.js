function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const jsonData = JSON.parse(this.responseText);

            mostrarDatos(jsonData);
        } else {
            alert("Hubo un error al hacer la petición");
        }
    };

    http.open('GET', url, true);
    http.send();
}

function mostrarDatos(data) {
    let res = document.getElementById('lista');
    res.innerHTML = "";

    for (const datos of data) {
        res.innerHTML += `
            <tr>
                <td class="columna1">${datos.id}</td>
                <td class="columna2">${datos.name}</td>
                <td class="columna3">${datos.username}</td>
                <td class="columna4">${datos.email}</td>
                <td class="columna5">${datos.address.street}, ${datos.address.suite}, ${datos.address.city}, ${datos.address.zipcode}</td>
                <td class="columna6">${datos.phone}</td>
                <td class="columna7">${datos.website}</td>
                <td class="columna8">${datos.company.name}</td>
            </tr>`;
    }
}

function buscarPorId() {
    const inputId = document.getElementById('inputId').value;
    if (inputId.trim() === "") {
        alert("Por favor, ingrese un ID para buscar.");
        return;
    }

    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/users/${inputId}`;

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            const jsonData = JSON.parse(this.responseText);
            mostrarDatos([jsonData]);
        } else if (this.status == 404) {
            alert("Usuario no encontrado. Por favor, ingrese un ID válido.");
        } else {
            alert("Hubo un error al hacer la petición");
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});

document.getElementById("btnBuscar").addEventListener('click', buscarPorId);