function buscarPais() {
    const countryName = document.getElementById('countryName').value;

    // Verificar si el nombre del país ingresado es válido
    if (countryName) {
        // Realizar la petición a la API de restcountries.com
        fetch(`https://restcountries.com/v3.1/name/${countryName}`)
            .then(response => {
                // Verificar si la respuesta es exitosa (código 200)
                if (!response.ok) {
                    throw new Error('Error en la respuesta de la API');
                }
                return response.json();
            })
            .then(data => mostrarInformacionPais(data))
            .catch(error => console.error('Error al obtener la información del país', error));
    } else {
        alert('Ingrese un nombre de país válido.');
    }
}

function mostrarInformacionPais(pais) {
    const countryInfoContainer = document.getElementById('countryInfo');
    // Se asume que la API devuelve un arreglo, tomamos el primer elemento
    const primerPais = pais[0];
    countryInfoContainer.innerHTML = `
        <h2>Información del País</h2>
        <p><strong>Nombre del País:</strong> ${primerPais.name.common}</p>
        <p><strong>Capital:</strong> ${primerPais.capital}</p>
        <p><strong>Lenguaje:</strong> ${Object.values(primerPais.languages).join(', ')}</p>
    `;
}

function limpiarInfo() {
    const countryInfoContainer = document.getElementById('countryInfo');
    countryInfoContainer.innerHTML = '';
    document.getElementById('countryName').value = '';
}
